filename = 'daftar negara.txt'
output_filename = 'daftar negara.json'
file = ''
with open(filename, 'r') as f:
    file = f.readlines()

file = [x.strip() for x in file]

as_json = '{"daftar": ['
for f in file:
    print(f)
    as_json += '{"item": "' + f + '"},'
as_json += ']}'

with open(output_filename, 'w') as f:
    f.write(as_json)

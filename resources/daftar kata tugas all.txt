# Dari Buku Tata Bahasa Baku Bahasa Indonesia Edisi Ketiga Bab Viii

- Preposisi
Akan
Antara
Bagi
Buat
Dari
Demi
Dengan
Di
Hingga
Ke
Kecuali
Lepas
Lewat
Oleh
Pada
Per
Peri
Sampai
Sejak
Semenjak
Seperti
Serta
Tanpa
Tentang
Untuk

-- Preposisi Berafiks
--- Prefiks
Bersama
Beserta
Menjelang
Menuju
Menurut
Seantero
Sekeliling
Sekitar
Selama
Sepanjang
Seputar
Seluruh
Terhadap

--- Sufiks
Bagaikan

--- Prefiks Sufiks
Melalui
Mengenai

-- Preposisi Berdampingan
Daripada
Kepada
Oleh Karena
Oleh Sebab
Sampai Kepada
Sampai Dengan
Selain Dari

-- Semantis Preposisi
--- Hubungan Tempat
Di
Ke
Dari
Hingga
Sampai
Antara
Pada

--- Peruntukan
Bagi
Untuk
Buat
Guna

--- Sebab
Karena
Sebab
Lantaran

--- Kesetaraan
Dengan
Sambil
Beserta
Bersama

--- Hubungan Pelaku
Oleh

--- Hubungan Waktu
Pada
Hingga
Sampai
Sejak
Semenjak
Menjelang

--- Hubungan Peristiwa
Tentang
Mengenai

--- Hubungan Milik
Dari


- Konjungtor
-- Konjungtor Koordinatif
Dan
Serta
Atau
Tetapi
Melainkan
Padahal
Sedangkan

-- Konjungtor Subordinatif
--- Waktu
Sejak
Semenjak
Sedari
Sewaktu
Ketika
Tatkala
Sementara
Begitu
Seraya
Selagi
Selama
Serta
Sambil
Demi
Setelah
Sesudah
Sebelum
Sehabis
Selesai
Seusai
Hingga
Sampai

--- Syarat
Jika
Jikalau
Kalau
Asal
Asalkan
Bila
Manakala

--- Pengandaian
Andaikan
Seandainya
Umpamanya
Sekiranya

--- Tujuan
Agar
Supaya
Biar

--- Konsensif
Biarpun
Meski
Meskipun
Walau
Walaupun
Sekalipun
Sungguhpun
Kendatipun

--- Pembandingan
Seakan-Akan
Seolah-Olah
Sebagaimana
Seperti
Sebagai
Laksana
Ibarat
Daripada
Alih-Alih

--- Sebab
Sebab
Karena
Oleh Karena
Oleh Sebab

--- Hasil
Sehingga
Sampai
Maka
Makanya

--- Alat
Dengan
Tanpa

--- Cara
Dengan
Tanpa

--- Komplementasi
Bahwa

--- Atributif
Yang

--- Perbandingan

-- Konjungtor Antarkalimat
Biarpun Demikian
Biarpun Begitu
Sekalipun Demikian
Sekalipun Begitu
Walaupun Demikian
Walaupun Begitu
Meskipun Demikian
Meskipun Begitu
Sungguhpun Demikian
Sungguhpun Begitu
Kemudian
Sesudah Itu
Setelah Itu
Selanjutnya
Tambahan Pula
Lagi Pula
Selain Itu
Sebaliknya
Sesungguhnya
Bahwasanya
Malah
Malahan
Bahkan
Tetapi
Akan Tetapi
Namun
Kecuali Itu
Dengan Demikian
Oleh Karena Itu
Oleh Sebab Itu
Sebelum Itu

- Interjeksi

-- Kejijikan
Bah
Cih
Cis
Ih
Idih

-- Kekesalan
Brengsek
Sialan
Buset
Keparat

-- Kekaguman Atau Kepuasan
Aduhai
Amboi
Asyik

-- Syukur
Alhamdulillah

-- Harapan
Insya Allah

-- Keheranan
Aduh
Aih
Ai
Lo
Duilah
Eh
Oh
Ah

-- Kekagetan
Astaga
Astagfirullah
Masyaallah

-- Ajakan
Ayo
Mari

-- Panggilan
Hai
He
Eh
Halo

-- Simpulan
Nah

- Artikula
Sang
Sri
Hang
Dang
Si